package com.springboot.validatepin.repository;

import com.springboot.validatepin.model.ValidatePinModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ValidatePinRepository extends JpaRepository<ValidatePinModel, Long> {

}
