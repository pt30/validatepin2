package com.springboot.validatepin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.client.RootUriTemplateHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplateHandler;

@SpringBootApplication
public class ValidatePinApplication{

   /* @Bean
    public RestTemplate getRestTemplate(RestTemplateBuilder builder) {
        UriTemplateHandler uriTemplateHandler = new RootUriTemplateHandler("https://setyawanh99.smockin.com/rpl/invoke");
        return builder
                .uriTemplateHandler(uriTemplateHandler)
                .build();
    }*/

	public static void main(String[] args) {
		SpringApplication.run(ValidatePinApplication.class, args);
	}

   /* @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }*/

}
