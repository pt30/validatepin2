package com.springboot.validatepin.controller;

import com.springboot.validatepin.model.ValidatePinModel;
import com.springboot.validatepin.repository.ValidatePinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.Arrays;

@RestController
public class ValidatePinController {

    @Autowired
    private ValidatePinRepository validatePinRepository;

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(value = "/validatepin")
    public String getDealerPinList() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);

        return restTemplate.exchange(
                "https://setyawanh99.smockin.com/rpl/invoke", HttpMethod.GET, entity, String.class).getBody();
    }
    @RequestMapping(value = "/validatepin/", method = RequestMethod.POST)
    @ResponseBody
    @ResponseStatus( HttpStatus.OK )
    public Long createDealerPin(@Valid @RequestBody ValidatePinModel ValidatePinModel){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<ValidatePinModel> entity = new HttpEntity<ValidatePinModel>(ValidatePinModel,headers);

        return restTemplate.exchange(
                "https://setyawanh99.smockin.com/rpl/invoke", HttpMethod.POST, entity, Long.class).getBody();
    }

}
